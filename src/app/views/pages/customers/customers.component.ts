import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTable } from "simple-datatables";
import { CommonService } from "../../../services/common.service";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  page = 1;
  pageSize = 10;
  totalCount: number;
  basicModalCloseResult: string = '';
  datasource:any =[];
  createtab: boolean = false;
  edittab : boolean = false;
  public attrForm: FormGroup;
  subcategory:any= [];
  attributedata:any=[];
  SubmitButtonName:string ='Save';
  commom_attributes_id: string = '';
  getDeleteCatId: any;
  CustomerData: any;
  constructor(private modalService: NgbModal,private commonService: CommonService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    debugger;
    //const dataTable = new DataTable("#CustomerTable");
    this.SubmitButtonName = 'Save';
    this.createtab = false;
    this.commom_attributes_id='';
    this.attrForm = this.formBuilder.group({
      'Name': ['', Validators.required],
      'attribute_description':['',Validators.required],
      'is_search_preference': ['', [Validators.required]],
      'is_global_search': ['', [Validators.required]],
      'type_Id': ['', [Validators.required]],
      "input_type": ['', [Validators.required]],
      "form_display_length":['',[Validators.required]],
      "is_active": ['', [Validators.required]]
    });
    this.categorydata();
  }

  categorydata(){
    this.commonService.getData("", "admin/users?page_no=" + this.page, "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          debugger;
          this.datasource = response.data.data;  
          this.totalCount = response.data.count;        
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)       
      console.log('getcountry err-->', err);
    });
  }

  openCategoryAttr(){
    this.createtab = true;    
    this.commom_attributes_id='';
  }

  backmenu(){
    this.createtab = false;
  }

  pageChanged(event) {
    this.page = event;
    this.categorydata();
  }

  editCategoryAttr(element) {
    this.createtab = true;
    this.commom_attributes_id = '1';
    this.CustomerData = element;
  }

  onSubmit() {
    if(this.attrForm.invalid){
      return;
    }   
    if (this.attrForm.valid) {
      this.SubmitButtonName = "Loading....";
      const result = Object.assign({}, this.attrForm.value);
      if(result.is_active == 'Active'){
        result.is_active = true;
      }else{
        result.is_active = false;
      }

      if(result.is_search_preference =='Yes'){
        result.is_search_preference = true;
      }else{
        result.is_search_preference = false;
      }  
      
      if(result.is_global_search =='Yes'){
        result.is_global_search = true;
      }else{
        result.is_global_search = false;
      }
      
        this.commonService.getData(result, `admin/attribute_master`, "PPOST").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {            
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  onUpdSubmit() {
    if(this.attrForm.invalid){
      return;
    }   
    if (this.attrForm.valid) {
      this.SubmitButtonName = "Loading....";
      const result = Object.assign({}, this.attrForm.value);
      if(result.is_active == 'Active'){
        result.is_active = true;
      }else{
        result.is_active = false;
      }

      if(result.is_search_preference =='Yes'){
        result.is_search_preference = true;
      }else{
        result.is_search_preference = false;
      }

      if(result.is_global_search =='Yes'){
        result.is_global_search = true;
      }else{
        result.is_global_search = false;
      }
      
      result.attribute_id = this.commom_attributes_id;
        this.commonService.getData(result, `admin/attribute_master`, "PPATCH").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {            
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  deleteCategoryModal(content, values) {

    this.openCategory(content);
    this.getDeleteCatId = values.attribute_id; 
  }

  openCategory(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

  deleteCategory() {

    this.commonService.getData("", `admin/attribute_master/${this.getDeleteCatId}`, "PDELETE").subscribe((response: any) => {
      try {
        if (response.success) {
          this.ngOnInit();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });

  }

  openCommonAttr(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

  returnZero() {
    return 0
  }

}
