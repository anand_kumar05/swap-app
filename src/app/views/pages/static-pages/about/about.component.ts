import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  htmlText = `<p> If You Can Think It, You Can Do It. </p>`
  quillConfig = {
    toolbar: {
      container: [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['code-block'],
        //  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
        [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
        //  [{ 'direction': 'rtl' }],                         // text direction

        //  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'align': [] }],

        //  ['clean'],                                         // remove formatting button

        //  ['link'],
        ['link', 'image', 'video']
      ],
    },
  }

  data: any = [];
  content_type: string;
  form: FormGroup;
  BtnName:string = "Update";
  constructor(private commonService: CommonService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.content_type = "about";
    this.form = this.formBuilder.group({
      page_detail_Id: [0],
      content1: ['', Validators.required],
      content2: [''],
      content3: [''],
      content_type: [this.content_type, Validators.required]
    })
    this.termsCondition();
  }

  termsCondition() {
    this.data = [];
    this.commonService.getData("", "admin/page_details?limit=10&content_type=" + this.content_type, "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.data = response.data.data;
          console.log(this.data);
          if (this.data.length > 0) {
            this.form.get("page_detail_Id")?.setValue(this.data[0].page_detail_Id);
            this.form.get("content1")?.setValue(this.data[0].content1);
            this.form.get("content2")?.setValue(this.data[0].content2);
            this.form.get("content3")?.setValue(this.data[0].content3);
          }
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)
        console.log('getcountry err-->', err);
    });
  }

  onSubmit() {
    this.BtnName = "Loading....";
    this.commonService.getData(this.form.value, "admin/page_details", "PATCH").subscribe((response: any) => {
      try {
        this.BtnName = "Update";
        if (response.success) {
          this.commonService.showAlert("success", "About Details is updated successfully", 'success');
       
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)
        console.log('getcountry err-->', err);
    });
  }

  onSelectionChanged = (event) => {
    if (event.oldRange == null) {
      this.onFocus();
    }
    if (event.range == null) {
      this.onBlur();
    }
  }

  onContentChanged = (event) => {
    // console.log(event.html);
  }

  onFocus = () => {
    console.log("On Focus");
  }
  onBlur = () => {
    console.log("Blurred");
  }

}
