import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from "../../../services/common.service";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss']
})
export class PackageComponent implements OnInit {

  page = 1;
  pageSize = 10;
  totalCount: number;
  basicModalCloseResult: string = '';
  datasource:any =[];
  createtab: boolean = false;
  edittab : boolean = false;
  public attrForm: FormGroup;
  subcategory:any= [];
  attributedata:any=[];
  SubmitButtonName:string ='Save';
  commom_attributes_id: string = '';
  getDeleteCatId: any;
  categoryListData: any;

  constructor(private modalService: NgbModal,private commonService: CommonService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    //const dataTable = new DataTable("#SubCategoryTable");
    this.SubmitButtonName = 'Save';
    this.createtab = false;
    this.commom_attributes_id='';
    this.attrForm = this.formBuilder.group({
      'package_name': ['', Validators.required],
      'categoryid':['',Validators.required],
      'subcategoryid': ['', [Validators.required]],
      'validity': ['', [Validators.required]],
      'product_limit': ['', [Validators.required]],
      "price": ['', [Validators.required]],
      "isactive": ['', [Validators.required]]
    });
    this.categorydata();
    this.getCategoryList();
  }

  categorydata(){
    this.commonService.getData("", "admin/package?page_no=" + this.page, "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.datasource = response.data.data;  
          this.totalCount = response.data.count;        
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)       
      console.log('getcountry err-->', err);
    });
  }

  getCategoryList() {
    this.commonService.getData("", `admin/category`, "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.categoryListData = response.data.data;
        }
      } catch (err) {

      }
    }, (err) => {
      console.log('Categorylist data err-->', err);
    })

  }

  onchgcategory(event) {
    this.subcategory = [];
    if (event != '') {
      const result = { category_id: event };
      this.commonService.getData(result, `admin/subcategory`, "POST").subscribe((response: any) => {
        try {
          if (response.success == true) {
            this.subcategory = response.data.data;
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  openCategoryAttr(){
    this.createtab = true;    
    this.commom_attributes_id='';
    this.attrForm.reset();
  }

  backmenu(){
    this.createtab = false;
  }

  pageChanged(event) {
    this.page = event;
    this.categorydata();
  }

  editCategoryAttr(element) {
    this.createtab = true;
    this.commom_attributes_id = element.package_id;
    debugger;
    this.onchgcategory(element.categoryid);
    for (var key in element) {
      const formControl = this.attrForm.get(key);
      if (formControl) {
        if (key != 'isactive') {
          this.attrForm.get(key).setValue(element[key]);
        }
        else if (key == 'isactive') {
          var value = element.isactive == true ? 'Active' : 'Draft';
          this.attrForm.get(key).setValue(value);
        }
      }
    }
  }

  onSubmit() {
    if(this.attrForm.invalid){
      return;
    }   
    if (this.attrForm.valid) {
      this.SubmitButtonName = "Loading....";
      const result = Object.assign({}, this.attrForm.value);
      if(result.isactive == 'Active'){
        result.isactive = true;
      }else{
        result.isactive = false;
      }
      
        this.commonService.getData(result, `admin/package`, "PPOST").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {            
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  onUpdSubmit() {
    if(this.attrForm.invalid){
      return;
    }   
    if (this.attrForm.valid) {
      this.SubmitButtonName = "Loading....";
      const result = Object.assign({}, this.attrForm.value);
      if(result.isactive == 'Active'){
        result.isactive = true;
      }else{
        result.isactive = false;
      }
    
      result.package_id = this.commom_attributes_id;
        this.commonService.getData(result, `admin/package`, "PPATCH").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {            
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  deleteCategoryModal(content, values) {

    this.openCategory(content);
    this.getDeleteCatId = values.attribute_id; 
  }

  openCategory(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

  deleteCategory() {

    this.commonService.getData("", `admin/package/${this.getDeleteCatId}`, "PDELETE").subscribe((response: any) => {
      try {
        if (response.success) {
          this.ngOnInit();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });

  }

  openCommonAttr(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }  

}

