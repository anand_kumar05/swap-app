import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTable } from "simple-datatables";
import { CommonService } from 'src/app/services/common.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categoryForm: FormGroup;
  basicModalCloseResult: string = '';
  page = 1;
  pageSize = 10;
  totalCount: number;
  submitted = false;
  buttonText: string = 'save';
  getCatId: any;
  getDeleteCatId: any;

  constructor(
    private modalService: NgbModal,
    private commonservice: CommonService,
    private formBuilder: FormBuilder
  ) { }
  categoryListData: any;

  ngOnInit(): void {
    
    this.getformBuilders();
    this.getCategoryList();
  }

  getformBuilders() {

    this.categoryForm = this.formBuilder.group({
      categoryName: ['', Validators.required],
      categorystatus: ['', Validators.required],
    });
  }

  get f() { return this.categoryForm.controls; }

  getCategoryList() {

    this.commonservice.getData("", `admin/category?is_active=true&page_no=${this.page}`, "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.categoryListData = response.data.data;
          this.totalCount = response.data.count;
        }
      } catch (err) {

      }
    }, (err) => {
      console.log('Categorylist data err-->', err);
    })

  }

  openCategory(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

  onSubmit() {

    this.submitted = true;

    if (this.categoryForm.invalid) {
      return;
    }
    const val = {
      "category_name": this.categoryForm.controls['categoryName'].value,
      "is_active": this.categoryForm.controls['categorystatus'].value,      
      font_awesome_class: "string"
    }

    if (this.buttonText === 'save') {
      this.addCategory(val)
    } else if (this.buttonText === 'update') {
      this.editCategory();
    } else {

    }   

    
  }

  addCategory(val) {

    this.commonservice.getData(val, `admin/category`, "POST").subscribe((response: any) => {
      try {
        console.log('category add')
        if (response.success) {
          this.getCategoryList();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {
      alert('Try Again');
      console.log('categorylist data err-->', err);
    });
  }

  editCategory() {

    const editVal = {      
        category_name: this.categoryForm.controls['categoryName'].value,
        font_awesome_class: "string",
        is_active: this.categoryForm.controls['categorystatus'].value, 
        category_id: this.getCatId     
    }

    this.commonservice.getData(editVal, `admin/category`, "PATCH").subscribe((response: any) => {
      try {
        if (response.success) {
          this.getCategoryList();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });
  }

  createCategory(content) {

    this.submitted = false;
    this.buttonText = 'save'
    this.openCategory(content);
    this.categoryForm.patchValue({
      categoryName: '',
      categorystatus: ''
    });
    // this.categoryForm.reset();
    // this.getformBuilders();
  }

  updateCategory(content, values) {

    console.log('qwe--',values);
    this.buttonText = 'update'
    this.openCategory(content);
    this.categoryForm.patchValue({
      categoryName: values.category_name,
      categorystatus: values.is_active
    });
    this.getCatId = values.category_id;    

  }

  deleteCategoryModal(content, values) {

    this.openCategory(content);
    this.getDeleteCatId = values.category_id; 
  }

  deleteCategory() {

    this.commonservice.getData("", `admin/category/${this.getDeleteCatId}`, "DELETE").subscribe((response: any) => {
      try {
        if (response.success) {
          this.getCategoryList();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });

  }
  pageChanged(event) {
    this.page = event;
    this.getCategoryList();
  }

}
