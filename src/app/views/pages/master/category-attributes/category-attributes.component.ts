import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTable } from "simple-datatables";
import { CommonService } from "../../../../services/common.service";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-category-attributes',
  templateUrl: './category-attributes.component.html',
  styleUrls: ['./category-attributes.component.scss']
})
export class CategoryAttributesComponent implements OnInit {
  page = 1;
  pageSize = 10;
  totalCount: number;
  basicModalCloseResult: string = '';
  datasource:any = [];
  createtab: boolean = false;
  subcategory:any= [];
  attributedata:any=[];
  public categoryForm: FormGroup;
  SubmitButtonName: string='Submit';
  parentenable:boolean = false;
  parentattr:any = [];
  commom_attributes_id: string = '';
  getDeleteCatId: any;

  constructor(private modalService: NgbModal,private commonService: CommonService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {    
    //const dataTable = new DataTable("#CategoryAttrTable");
    this.SubmitButtonName = 'Submit';
    this.categoryForm = this.formBuilder.group({
      'sub_category_id': ['', Validators.required],
      'attributes': this.formBuilder.array([
        this.formBuilder.group({
          'attribute_id': ['', [Validators.required]],
          'is_mandatory': ['', [Validators.required]],
          "order_no": ['', [Validators.required]],
          "type_id": ['', [Validators.required]],
          "display_order_no": ['', [Validators.required]],
          "Is_left_filter": ['', [Validators.required]],
          "is_parent": ['', [Validators.required]],          
          "is_popular_search": ['', [Validators.required]],
          "Is_active": ['', [Validators.required]],
          "parent_attribute_id": ['', [Validators.required]]
        })
      ])
    });
    this.categorydata();    
    this.subcategorydata();
    this.attributefun();
  }  

  categorydata(){
    this.commonService.getData('', "admin/category_attribute_master", "PGET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.datasource = response.data.data; 
          this.totalCount = response.data.count;         
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)       
      console.log('getcountry err-->', err);
    });
  }

  subcategorydata(){
    this.commonService.getData("","admin/subcategory?limit=1000", "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.subcategory = response.data.data;
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)       
      console.log('getcountry err-->', err);
    });
  }

  attributefun(){
    this.commonService.getData("", "admin/attribute_master?limit=10000", "PGET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.attributedata = response.data.data;  
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)       
      console.log('getcountry err-->', err);
    });
  }

  // openCategoryAttr(content) {
  //   this.modalService.open(content, { size: 'lg' }).result.then((result) => {
  //     this.basicModalCloseResult = "Modal closed" + result;
  //   }).catch((res) => { });
  // }

  openCategoryAttr(){
    this.createtab = true;    
  }

  backmenu(){
    this.createtab = false;
  }

  initFormArray() {
    return this.formBuilder.group({
      'attribute_id': ['', [Validators.required]],
      'is_mandatory': ['', [Validators.required]],
      "order_no": ['', [Validators.required]],
      "type_id": ['', [Validators.required]],
      "display_order_no": ['', [Validators.required]],
      "Is_left_filter": ['', [Validators.required]],
      "is_parent": ['', [Validators.required]],
      "is_popular_search": ['', [Validators.required]],
      "Is_active": ['', [Validators.required]],
      "parent_attribute_id": ['', [Validators.required]]
    });
  }

  removeFormArray(i) {
    const control = <FormArray>this.categoryForm.controls['attributes'];
    control.removeAt(i);
  }

  
  addFormArray() {
    // if(this.categoryForm.invalid){
    //   return;
    // }
    const control = <FormArray>this.categoryForm.controls['attributes'];
    control.push(this.initFormArray());
  }

  
  get resultSetDetails() {
    return <FormArray>this.categoryForm.get('attributes');
  }  

  chgchild(event){
    this.parentenable = true;
    const DynamicFormArray = <FormArray>this.categoryForm.get('attributes');
    DynamicFormArray.controls[0].get('parent_attribute_id').setValue('');
    if(event == 'Yes'){
      this.parentenable = true;
    }else{
      this.parentenable = false;
    }
  }

  onchgattribute(event){
    if(event !=''){
      const result = {sub_category_id : event};
      this.commonService.getData(result, `admin/category_attribute_master/list`, "PPOST").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {            
            this.parentattr = response.data.data;
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  onSubmit() {
    if(this.categoryForm.invalid){
      return;
    }   
    if (this.categoryForm.valid) {
      this.SubmitButtonName = "Loading....";
      const result = Object.assign({}, this.categoryForm.value);
      if(result.attributes[0].Is_active == 'Active'){
        result.attributes[0].Is_active = true;
      }else{
        result.attributes[0].Is_active = false;
      }

      if(result.attributes[0].is_mandatory =='Yes'){
        result.attributes[0].is_mandatory = true;
      }else{
        result.attributes[0].is_mandatory = false;
      }

      if(result.attributes[0].Is_left_filter =='Yes'){
        result.attributes[0].Is_left_filter = true;
      }else{
        result.attributes[0].Is_left_filter = false;
      }

      if(result.attributes[0].is_parent =='Yes'){
        result.attributes[0].is_parent = true;
      }else{
        result.attributes[0].is_parent = false;
      }

      if(result.attributes[0].is_popular_search =='Yes'){
        result.attributes[0].is_popular_search = true;
      }else{
        result.attributes[0].is_popular_search = false;
      }
      this.commonService.getData(result, "​/admin​/category_attribute_master", "PPOST").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.IsSuccess == true) {            
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  pageChanged(event) {
    this.page = event;
    this.categorydata();
  }

  editCategoryAttr(element) {
    this.createtab = true;
    this.commom_attributes_id = element.sub_category_id;
    // for (var key in element) {
    //   const formControl = this.categoryForm.get(key);
    //   if (formControl) {
    //     if (key == 'sub_category_id') {
    //       this.categoryForm.get(key).setValue(element[key]);
    //     }       
    //   }
    // }
    this.categoryForm.get('sub_category_id').setValue(element.sub_category_id);
    this.AddDataToSubForm(element.attributes);
  }

  AddDataToSubForm(element) {         
    const DynamicFormArray = <FormArray>this.categoryForm.get('attributes');

    for (let i = 0; i < element.length; i++) {

      if (i != 0) {
        this.addFormArray();
      }

      DynamicFormArray.controls[i].get('attribute_id').setValue(element[i].attribute_id);      

      DynamicFormArray.controls[i].get('order_no').setValue(element[i].order_no);

      DynamicFormArray.controls[i].get('type_id').setValue(element[i].type_id);

      DynamicFormArray.controls[i].get('display_order_no').setValue(element[i].display_order_no);      

      DynamicFormArray.controls[i].get('is_parent').setValue(element[i].is_parent); 

      DynamicFormArray.controls[i].get('parent_attribute_id').setValue(element[i].parent_attribute_id);

      if (element[i].Is_left_filter != '') {
        var value = element[i].Is_left_filter == 1 ? 'Yes' : 'No';
        DynamicFormArray.controls[i].get('Is_left_filter').setValue(value);
      }

      if (element[i].is_parent != '') {
        var value = element[i].is_parent == true ? 'Yes' : 'No';
        if(value == 'Yes'){
          this.parentenable = true;
          this.onchgattribute(this.commom_attributes_id);
        }
        DynamicFormArray.controls[i].get('is_parent').setValue(value);
      }
      
      if (element[i].is_popular_search != '') {
        var value = element[i].is_popular_search == 1 ? 'Yes' : 'No';
        DynamicFormArray.controls[i].get('is_popular_search').setValue(value);
      }

      if (element[i].Is_active != '') {
        var value = element[i].Is_active == 1 ? 'Active' : 'Draft';
        DynamicFormArray.controls[i].get('Is_active').setValue(value);
      }
      if (element[i].is_mandatory != '') {
        var value = element[i].is_mandatory == 1 ? 'Yes' : 'No';
        DynamicFormArray.controls[i].get('is_mandatory').setValue(value);
      }

      // if (this.channeldynamicsubformfields[i].name == 'Lot' && this.defaultlot != null) {
      //   DynamicFormArray.controls[i].get('value').setValue(this.defaultlot);
      // } else {
      //   DynamicFormArray.controls[i].get('value').setValue(this.channeldynamicsubformfields[i].value);
      // }      
    }
  }

  onUpdSubmit() {
    if(this.categoryForm.invalid){
      return;
    }   
    if (this.categoryForm.valid) {
      const result = Object.assign({}, this.categoryForm.value);

      if(result.attributes[0].Is_active == 'Active'){
        result.attributes[0].Is_active = true;
      }else{
        result.attributes[0].Is_active = false;
      }

      if(result.attributes[0].is_mandatory =='Yes'){
        result.attributes[0].is_mandatory = true;
      }else{
        result.attributes[0].is_mandatory = false;
      }

      if(result.attributes[0].Is_left_filter =='Yes'){
        result.attributes[0].Is_left_filter = true;
      }else{
        result.attributes[0].Is_left_filter = false;
      }

      if(result.attributes[0].is_parent =='Yes'){
        result.attributes[0].is_parent = true;
      }else{
        result.attributes[0].is_parent = false;
      }

      if(result.attributes[0].is_popular_search =='Yes'){
        result.attributes[0].is_popular_search = true;
      }else{
        result.attributes[0].is_popular_search = false;
      }
    
      result.sub_category_id = this.commom_attributes_id;
        this.commonService.getData(result, `admin/common_attribute_master`, "PPATCH").subscribe((response: any) => {
        try {
          if (response.success == true) {            
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
      });
    }
  }

  deleteCategoryModal(content, values) {

    this.openCategory(content);
    this.getDeleteCatId = values.sub_category_id; 
  }

  openCategory(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

  deleteCategory() {

    this.commonService.getData("", `admin/category_attribute_master/${this.getDeleteCatId}`, "PDELETE").subscribe((response: any) => {
      try {
        if (response.success) {
          this.ngOnInit();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });

  }

  openCommonAttr(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }  

}
