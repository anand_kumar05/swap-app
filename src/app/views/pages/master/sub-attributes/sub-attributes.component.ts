import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTable } from "simple-datatables";
import { CommonService } from "../../../../services/common.service";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-sub-attributes',
  templateUrl: './sub-attributes.component.html',
  styleUrls: ['./sub-attributes.component.scss']
})
export class SubAttributesComponent implements OnInit {
  page = 1;
  pageSize = 10;
  totalCount: number;
  basicModalCloseResult: string = '';
  datasource: any = [];
  createtab: boolean = false;
  public subcategoryForm: FormGroup;
  subcategory: any = [];
  attributedata: any = [];
  SubmitButtonName: string = 'Submit';
  parentenable: boolean = false;
  parentattr: any = [];
  commom_attributes_id: string = '';
  getDeleteCatId: any;
  constructor(private modalService: NgbModal, private commonService: CommonService, private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.createtab = false;
    //const dataTable = new DataTable("#SubCategoryTable");
    this.subcategoryForm = this.formBuilder.group({
      'sub_attributes_name': ['', Validators.required],
      'sub_attributes_value': ['', [Validators.required]],      
      "attributes_id": ['', [Validators.required]],
      "sub_category_id": ['', [Validators.required]],
      "is_active": ['', Validators.required],
      "is_child": ['', [Validators.required]],
      'parent_id': ['']
    });
    this.categorydata();
    this.subcategorydata();
    this.attributefun();
  }

  categorydata() {
    this.commonService.getData("", "admin/sub_attribute_master?page_no=" + this.page, "PGET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.datasource = response.data.data;
          this.totalCount = response.data.count;
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)
        console.log('getcountry err-->', err);
    });
  }

  openCategoryAttr() {
    this.createtab = true;
    this.parentenable = false;
    this.commom_attributes_id='';
    this.subcategoryForm.reset();
  }

  backmenu() {
    this.createtab = false;
  }

  subcategorydata() {
    this.commonService.getData("", "admin/subcategory?limit=1000", "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.subcategory = response.data.data;
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)
        console.log('getcountry err-->', err);
    });
  }

  attributefun() {
    this.commonService.getData("", "admin/attribute_master", "PGET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.attributedata = response.data.data;
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)
        console.log('getcountry err-->', err);
    });
  }

  onchgattribute(event) {
    this.parentattr = [];
    if (event != '') {
      const result = { sub_category_id: event, is_active: true };
      this.commonService.getData(result, `admin/sub_attribute_master/list?limit=100000`, "PPOST").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {
            this.parentattr = response.data.data;
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  pageChanged(event) {
    this.page = event;
    this.categorydata();
  }

  onSubmit() {
    if (this.subcategoryForm.invalid) {
      return;
    }
    if (this.subcategoryForm.valid) {
      this.SubmitButtonName = "Loading....";
      const result = Object.assign({}, this.subcategoryForm.value);
      if (result.is_active == 'Active') {
        result.is_active = true;
      } else {
        result.is_active = false;
      }
      delete result.is_child;
      this.commonService.getData(result, `admin/sub_attribute_master`, "PPOST").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  onUpdSubmit() {
    if (this.subcategoryForm.invalid) {
      return;
    }
    if (this.subcategoryForm.valid) {
      const result = Object.assign({}, this.subcategoryForm.value);

      if (result.is_active == 'Active') {
        result.is_active = true;
      } else {
        result.is_active = false;
      }

      result.sub_attributes_id = this.commom_attributes_id;
      delete result.is_child;
      this.commonService.getData(result, `admin/sub_attribute_master`, "PPATCH").subscribe((response: any) => {
        try {
          debugger;
          if (response.success == true) {
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
      });
    }
  }

  chgchild(event) {    
    if (event == 'Yes') {
      this.parentenable = true;
    } else {
      this.parentenable = false;
      this.subcategoryForm.get('parent_id').setValue('');
    }
  }

  deleteCategoryModal(content, values) {

    this.openCategory(content);
    this.getDeleteCatId = values.sub_attributes_id; 
  }

  openCategory(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

  deleteCategory() {

    this.commonService.getData("", `admin/sub_attribute_master/${this.getDeleteCatId}`, "PDELETE").subscribe((response: any) => {
      try {
        if (response.success) {
          this.ngOnInit();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });

  }

  editCategoryAttr(element) {
    this.parentenable = false;
    this.subcategoryForm.reset();
    this.createtab = true;
    this.commom_attributes_id = element.sub_attributes_id;    
    for (var key in element) {
      const formControl = this.subcategoryForm.get(key);
      if (formControl) {
        if (key == 'parent_id') {
          debugger;
          if(element[key] != '' && element[key] != undefined){
            this.onchgattribute(element.sub_category_id);
            this.parentenable = true;
            this.subcategoryForm.get('is_child').setValue('Yes');
            this.subcategoryForm.get(key).setValue(element[key]);
          }else{
            this.subcategoryForm.get('is_child').setValue('No');
            this.subcategoryForm.get(key).setValue(element[key]);
          }
        }
        if (key != 'is_active') {
          this.subcategoryForm.get(key).setValue(element[key]);
        }        
        else if (key == 'is_active') {
          var value = element.is_active == true ? 'Active' : 'Draft';
          this.subcategoryForm.get(key).setValue(value);
        }
      }
    }
  }

}
