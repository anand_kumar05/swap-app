import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubAttributesComponent } from './sub-attributes.component';

describe('SubAttributesComponent', () => {
  let component: SubAttributesComponent;
  let fixture: ComponentFixture<SubAttributesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubAttributesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
