import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonAttributesComponent } from './common-attributes.component';

describe('CommonAttributesComponent', () => {
  let component: CommonAttributesComponent;
  let fixture: ComponentFixture<CommonAttributesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommonAttributesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
