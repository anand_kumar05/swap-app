import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTable } from "simple-datatables";
import { CommonService } from "../../../../services/common.service";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-common-attributes',
  templateUrl: './common-attributes.component.html',
  styleUrls: ['./common-attributes.component.scss']
})
export class CommonAttributesComponent implements OnInit {

  page = 1;
  pageSize = 10;
  totalCount: number;
  basicModalCloseResult: string = '';
  datasource:any =[];
  createtab: boolean = false;
  edittab : boolean = false;
  public commonattrForm: FormGroup;
  subcategory:any= [];
  attributedata:any=[];
  SubmitButtonName:string ='Save';
  commom_attributes_id: string = '';
  getDeleteCatId: any;
  constructor(private modalService: NgbModal,private commonService: CommonService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    //const dataTable = new DataTable("#SubCategoryTable");
    this.SubmitButtonName = 'Save';
    this.createtab = false;
    this.commom_attributes_id='';
    this.commonattrForm = this.formBuilder.group({
      'commom_attributes_name': ['', Validators.required],
      'is_mandatory':['',Validators.required],
      'column_name': ['', [Validators.required]],
      'input_type': ['', [Validators.required]],
      "display_order_no": ['', [Validators.required]],
      "form_display_length":['',[Validators.required]],
      "order_id": ['', [Validators.required]],
      "is_active": ['', [Validators.required]]
    });
    this.categorydata();
  }

  categorydata(){
    this.commonService.getData("", "admin/common_attribute_master?page_no=" + this.page, "PGET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.datasource = response.data.data;  
          this.totalCount = response.data.count;        
        }
      } catch (err) {

      }
    }, (err) => {
      if (err.status == 401)       
      console.log('getcountry err-->', err);
    });
  }

  openCategoryAttr(){
    this.createtab = true;    
    this.commom_attributes_id='';
  }

  backmenu(){
    this.createtab = false;
  }

  pageChanged(event) {
    this.page = event;
    this.categorydata();
  }

  editCategoryAttr(element) {
    this.createtab = true;
    this.commom_attributes_id = element.commom_attributes_id;
    for (var key in element) {
      const formControl = this.commonattrForm.get(key);
      if (formControl) {
        if (key != 'is_active' && key != 'is_mandatory') {
          this.commonattrForm.get(key).setValue(element[key]);
        }
        else if (key == 'is_active') {
          var value = element.is_active == true ? 'Active' : 'Draft';
          this.commonattrForm.get(key).setValue(value);
        }
        else if (key == 'is_mandatory') {
          var value = element.is_mandatory == true ? 'Yes' : 'No';
          this.commonattrForm.get(key).setValue(value);
        }
      }
    }
  }

  onSubmit() {
    if(this.commonattrForm.invalid){
      return;
    }   
    if (this.commonattrForm.valid) {
      this.SubmitButtonName = "Loading....";
      const result = Object.assign({}, this.commonattrForm.value);
      if(result.is_active == 'Active'){
        result.is_active = true;
      }else{
        result.is_active = false;
      }

      if(result.is_mandatory =='Yes'){
        result.is_mandatory = true;
      }else{
        result.is_mandatory = false;
      }
      //this.commonService.getData(this.commonattrForm.value, "​admin/common_attribute_master", "PPOST").subscribe((response: any) => {
        this.commonService.getData(result, `admin/common_attribute_master`, "PPOST").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {            
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  onUpdSubmit() {
    if(this.commonattrForm.invalid){
      return;
    }   
    if (this.commonattrForm.valid) {
      this.SubmitButtonName = "Loading....";
      const result = Object.assign({}, this.commonattrForm.value);
      if(result.is_active == 'Active'){
        result.is_active = true;
      }else{
        result.is_active = false;
      }

      if(result.is_mandatory =='Yes'){
        result.is_mandatory = true;
      }else{
        result.is_mandatory = false;
      }
      result.commom_attributes_id = this.commom_attributes_id;
        this.commonService.getData(result, `admin/common_attribute_master`, "PPATCH").subscribe((response: any) => {
        try {
          this.SubmitButtonName = "Save";
          if (response.success == true) {            
            this.ngOnInit();
          }
          else {
            //this.errorMsg = "Invalid Request";
          }
        } catch (err) {
          this.SubmitButtonName = "Save";
        }
      }, (err) => {
        console.log(err);
        //this.errorMsg = "Invalid Request";
        this.SubmitButtonName = "Save";
      });
    }
  }

  deleteCategoryModal(content, values) {

    this.openCategory(content);
    this.getDeleteCatId = values.commom_attributes_id; 
  }

  openCategory(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

  deleteCategory() {

    this.commonService.getData("", `admin/common_attribute_master/${this.getDeleteCatId}`, "PDELETE").subscribe((response: any) => {
      try {
        if (response.success) {
          this.ngOnInit();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });

  }

  openCommonAttr(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }  

}
