import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {

  subcategoryForm!: FormGroup;
  basicModalCloseResult: string = '';
  subCategoryListData: any;
  paginationPageSize = 10;
  categoryListData: any;
  page = 1;
  pageSize = 10;
  totalCount: number;
  submitted = false;
  buttonText: string = 'save';
  getSubCatId: any;
  getDeleteSubCatId: any;

  constructor(
    private modalService: NgbModal,
    private commonservice: CommonService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    // const dataTable = new DataTable("#SubCategoryTable");
    this.getformBuilders();
    this.getSubCategoryList();
    this.getCategoryList();
  }

  getformBuilders() {

    this.subcategoryForm = this.formBuilder.group({
      category_FC: ['', Validators.required],
      subCatStatus_FC: ['', Validators.required],
      sub_category_FC: ['', Validators.required],

    });
  }

  get f() { return this.subcategoryForm.controls; }

  getCategoryList() {
    this.commonservice.getData("", `admin/category`, "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.categoryListData = response.data.data;
        }
      } catch (err) {

      }
    }, (err) => {
      console.log('Categorylist data err-->', err);
    })

  }

  getSubCategoryList() {

    this.commonservice.getData("", `admin/subcategory?sub_is_active=true&page_no=${this.page}`, "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.subCategoryListData = response.data.data;
          this.totalCount = response.data.count;
        }
      } catch (err) {

      }
    }, (err) => {
      console.log('Categorylist data err-->', err);
    })
  }

  pageChanged(event) {

    this.page = event;
    this.getSubCategoryList();

  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.subcategoryForm.invalid) {
      return;
    }

    const val = {
      "category_id": this.subcategoryForm.controls['category_FC'].value,
      "sub_category_name": this.subcategoryForm.controls['sub_category_FC'].value,
      "sub_is_active": this.subcategoryForm.controls['subCatStatus_FC'].value,
      "font_awesome_class": "string"
    }

    if (this.buttonText === 'save') {
      this.addSubCategory(val)
    } else if (this.buttonText === 'update') {
      this.editSubCategory(val);
    } else {

    }    

  }

  addSubCategory(val) {

    this.commonservice.getData(val, `admin/subcategory`, "POST").subscribe((response: any) => {
      try {
        if (response.success) {
          this.getSubCategoryList();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {
      alert('Try Again');
      console.log('categorylist data err-->', err);
    });
  }

  editSubCategory(val) {

    const editVal = {
      category_id: this.subcategoryForm.controls['category_FC'].value,
      sub_category_id: this.getSubCatId,
      sub_category_name: this.subcategoryForm.controls['sub_category_FC'].value,
      is_active: true,
      sub_is_active: this.subcategoryForm.controls['subCatStatus_FC'].value,
      sub_font_awesome_class: "string",
    }
    this.commonservice.getData(editVal, `admin/subcategory`, "PATCH").subscribe((response: any) => {
      try {
        if (response.success) {
          this.getSubCategoryList();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });
  }

  openSubCategory(content) {

    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

  createSubcategory(content) {

    this.submitted = false;
    this.buttonText = 'save'
    this.openSubCategory(content);
    this.getformBuilders();
  }

  updateSubCategory(content, values) {

    console.log(values);
    this.buttonText = 'update'
    this.openSubCategory(content);
    this.subcategoryForm.patchValue({
      category_FC: values.category_id,
      sub_category_FC: values.sub_category_name,
      subCatStatus_FC: values.sub_is_active
    });
    this.getSubCatId = values.sub_category_id;    

  }

  deleteSubCategoryModal(content, values) {

    this.openSubCategory(content);
    this.getDeleteSubCatId = values.sub_category_id; 
  }

  deleteSubCategory() {

    this.commonservice.getData("", `admin/subcategory/${this.getDeleteSubCatId}`, "DELETE").subscribe((response: any) => {
      try {
        if (response.success) {
          this.getSubCategoryList();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {      
      console.log('categorylist data err-->', err);
    });

  }

}
