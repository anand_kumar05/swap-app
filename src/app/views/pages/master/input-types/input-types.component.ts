import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTable } from "simple-datatables";

@Component({
  selector: 'app-input-types',
  templateUrl: './input-types.component.html',
  styleUrls: ['./input-types.component.scss']
})
export class InputTypesComponent implements OnInit {

  basicModalCloseResult: string = '';

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    const dataTable = new DataTable("#SubCategoryTable");
  }

  openInputType(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.basicModalCloseResult = "Modal closed" + result;
    }).catch((res) => { });
  }

}
