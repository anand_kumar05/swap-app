import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PeoplesData, Person } from '../../../core/dummy-datas/peoples.data';
import { CommonService } from '../../../services/common.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss']
})
export class AccessComponent implements OnInit {

  adminForm: FormGroup;
  simpleItems = [];
  selectedSimpleItem: any = null;

  people: Person[] = [];
  selectedPersonId: string = null;

  selectedSearchPersonId: string = null;

  selectedPeople: any = null;

  groupedMultiSelectedPeople: any = null;

  customTemplateSelectedPeople: any = null;

  CloseModel: string = '';
  countryListData: any;
  modalReference: Promise<void>;

  constructor(
    private modalService: NgbModal,
    private commonservice: CommonService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {

    this.getformBuilders();
    this.simpleItems = [true, 'Two', 3];
    this.people = PeoplesData.peoples;
    this.getCountryList();
  }

  getformBuilders() {

    this.adminForm = this.formBuilder.group({
      countryFc: ['', Validators.required],
      emailFc: ['', Validators.required],
      mobileFc: ['', Validators.required],      
      swapFc: ['', Validators.required],
      rentFc: ['', Validators.required],
      sellFc: ['', Validators.required],
     
  });
  }
  // convenience getter for easy access to form fields
  get f() { return this.adminForm.controls; }

  openLoginAccess(content) {
    console.log(content);
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.CloseModel = "Modal closed" + result
      this.countryListData
    }).catch((res) => {});

    
  }

  getCountryList() {

    this.commonservice.getData("", `country`, "GET").subscribe((response: any) => {
      try {
        if (response.success) {
          this.countryListData = response.data;

        }
      } catch (err) {

      }
    }, (err) => {
      console.log('countrylist data err-->', err);
    });
  }

  fetchFormVal(val) {

    console.log('forms val-->',val);
    this.adminForm.controls['emailFc'].setValue(val.isEmailValid);
    this.adminForm.controls['mobileFc'].setValue(val.isMobileValid);
    this.adminForm.controls['swapFc'].setValue(val.isSwap);
    this.adminForm.controls['rentFc'].setValue(val.isRent);
    this.adminForm.controls['sellFc'].setValue(val.isSell);
    this.adminForm.controls['countryFc'].setValue(val.countryId);

  }

  updateAdmin() {

    const val = {
      "swap": this.adminForm.controls['swapFc'].value ? 1 : 0,
      "sell": this.adminForm.controls['sellFc'].value ? 1 : 0,
      "rent": this.adminForm.controls['rentFc'].value ? 1 : 0,
      "is_email_valid": this.adminForm.controls['emailFc'].value ? 1 : 0,
      "is_mobile_no_valid": this.adminForm.controls['mobileFc'].value ? 1 : 0,
      "theme": 0
    }
    console.log('sele val-->',val)
    const countryId = this.adminForm.controls['countryFc'].value;
    this.commonservice.getData(val, `admin/update/country/`+countryId, "POST").subscribe((response: any) => {
      try {
        if (response.success) {
          this.getCountryList();
          this.modalService.dismissAll();
        }
      } catch (err) {

      }
    }, (err) => {
      alert('Try Again');
      console.log('countrylist data err-->', err);
    });
      
  }

}
