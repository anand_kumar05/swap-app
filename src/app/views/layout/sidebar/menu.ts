import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
  {
    label: 'Main',
    isTitle: true
  },
  {
    label: 'Dashboard',
    icon: 'home',
    link: '/dashboard'
  },
  {
    label: 'Customers',
    icon: 'users',
    link: '/customers'
  },
  {
    label: 'Products',
    icon: 'box',
    link: '/products'
  },
  {
    label: 'Master',
    isTitle: true
  },
  {
    label: 'Category',
    icon: 'aperture',
    link: '/category',
  },
  {
    label: 'Sub Category',
    icon: 'corner-down-right',
    link: '/sub-category',
  },
  {
    label: 'Types',
    icon: 'type',
    link: '/types',
  },
  {
    label: 'Input Types',
    icon: 'edit-3',
    link: '/input-types',
  },
  {
    label: 'Attributes',
    icon: 'hash',
    link: '/attributes',
  },
  {
    label: 'Catrgory Attributes',
    icon: 'pen-tool',
    link: '/category-attributes',
  },
  {
    label: 'Common Attributes',
    icon: 'layers',
    link: '/common-attributes',
  },
  {
    label: 'Sub Attributes',
    icon: 'server',
    link: '/sub-attributes',
  },
  {
    label: 'Settings',
    isTitle: true
  },
  {
    label: 'Access',
    icon: 'unlock',
    link: '/access',
  },
  {
    label: 'Static Pages',
    isTitle: true
  },
  {
    label: 'About',
    icon: 'file-text',
    link: '/pages',
  },
  {
    label: 'Terms & Condition',
    icon: 'file-text',
    link: '/pages/terms-condition',
  },
  {
    label: 'Privacy Policy',
    icon: 'file-text',
    link: '/pages/privacy-policy',
  },
  {
    label: 'Support',
    icon: 'file-text',
    link: '/pages/support',
  },
  {
    label: 'Others',
    isTitle: true
  },
  {
    label: 'Sponsors',
    icon: 'hexagon',
    link: '/sponsors',
  },
  {
    label: 'Package',
    icon: 'package',
    link: '/package',
  },
];
