import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import Swal from 'sweetalert2';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  APIUrl = environment.API;
  PRODUCT_API_URL = environment.PRODUCTAPI;
  FileAPIUrl = environment.FileAPI;

  constructor(private http: HttpClient) { }

  // showToastMsg(type = "", message, title = "") {
  //   if (type == "success") {
  //     this.toastr.success(message, title);
  //   }
  //   else if (type == "error") {
  //     this.toastr.error(message, title);
  //   }
  //   else if (type == "warning") {
  //     this.toastr.warning(message, title);
  //   }
  //   else {
  //     this.toastr.info(message, title);
  //   }
  // }

  showAlert(title, message, actiontype, count = 800) {
    Swal.fire({
      title: title,
      text: message,
      icon: actiontype,
      showConfirmButton: false,
      timer: count
    })
  }

  getData(model, url, mode) {
    let auth_token = localStorage.getItem("SwapToken") || "";
    const headers: any = {
      'Content-Type': 'application/json'
    }
    const multiPart: any = {}
    if (auth_token) {
      headers.Authorization = `Bearer ${auth_token}`;
      multiPart.Authorization = `Bearer ${auth_token}`;
    }

    if (mode == "GET") {
      return this.http.get(this.APIUrl + url, httpOptions)
    } else if (mode == "POST") {
      console.log(" i amc check----------->>>",this.APIUrl + url)
      return this.http.post(this.APIUrl + url, JSON.stringify(model), { headers: new HttpHeaders(headers) });
    } else if (mode == "PATCH") {
      return this.http.patch(this.APIUrl + url, JSON.stringify(model), { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    } else if (mode == "DELETE") {
      return this.http.delete(this.APIUrl + url, { headers: new HttpHeaders(headers) });
    }
    else if (mode == "PGET") {
      return this.http.get(this.PRODUCT_API_URL + url, httpOptions)
    }
    else if (mode == "PPOST") {
      return this.http.post(this.PRODUCT_API_URL + url, JSON.stringify(model), { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }
    else if (mode == "PPATCH") {
      return this.http.patch(this.PRODUCT_API_URL + url, JSON.stringify(model), { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    } else if (mode == "PDELETE") {
      return this.http.delete(this.PRODUCT_API_URL + url, { headers: new HttpHeaders(headers) });
    }
  }
}
