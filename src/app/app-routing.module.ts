import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseComponent } from './views/layout/base/base.component';
import { AuthGuard } from './core/guard/auth.guard';


const routes: Routes = [
  { path: 'auth', loadChildren: () => import('./views/pages/auth/auth.module').then(m => m.AuthModule) },
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'access',
        loadChildren: () => import('./views/pages/access/access.module').then(m => m.AccessModule)
      },
      {
        path: 'customers',
        loadChildren: () => import('./views/pages/customers/customers.module').then(m => m.CustomersModule)
      },
      {
        path: 'products',
        loadChildren: () => import('./views/pages/products/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'sponsors',
        loadChildren: () => import('./views/pages/sponsored-ads/sponsored-ads.module').then(m => m.SponsoredAdsModule)
      },
      {
        path: 'package',
        loadChildren: () => import('./views/pages/package/package.module').then(m => m.PackageModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./views/pages/master/category/category.module').then(m => m.CategoryModule)
      },
      {
        path: 'sub-category',
        loadChildren: () => import('./views/pages/master/sub-category/sub-category.module').then(m => m.SubCategoryModule)
      },
      {
        path: 'types',
        loadChildren: () => import('./views/pages/master/types/types.module').then(m => m.TypesModule)
      },
      {
        path: 'input-types',
        loadChildren: () => import('./views/pages/master/input-types/input-types.module').then(m => m.InputTypesModule)
      },
      {
        path: 'attributes',
        loadChildren: () => import('./views/pages/master/attributes/attributes.module').then(m => m.AttributesModule)
      },
      {
        path: 'category-attributes',
        loadChildren: () => import('./views/pages/master/category-attributes/category-attributes.module').then(m => m.CategoryAttributesModule)
      },
      {
        path: 'common-attributes',
        loadChildren: () => import('./views/pages/master/common-attributes/common-attributes.module').then(m => m.CommonAttributesModule)
      },
      {
        path: 'sub-attributes',
        loadChildren: () => import('./views/pages/master/sub-attributes/sub-attributes.module').then(m => m.SubAttributesModule)
      },
      {
        path: 'pages',
        loadChildren: () => import('./views/pages/static-pages/static-pages.module').then(m => m.StaticPagesModule)
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      // { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
