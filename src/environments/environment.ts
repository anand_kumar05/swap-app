// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API: "http://localhost:80/",  
  PRODUCTAPI: "http://localhost:4000/",
  FileAPI: "http://172.23.3.97/PMSHR/Server/",
  ExternalAPI: "http://www.geoplugin.net/json.gp?ip=2409:4072:e8b:f84d:993d:dd8:3d90:5f5d",
  url: '',
  IMAGEURL: 'http://localhost:3000/product/image/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
