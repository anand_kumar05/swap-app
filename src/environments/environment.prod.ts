export const environment = {
  production: true,
  API: "https://swap-rent-api-5a9ot.ondigitalocean.app/",  
  PRODUCTAPI: "https://swap-rent-product-api-h8zru.ondigitalocean.app/",
  FileAPI: "http://172.23.3.97/PMSHR/Server/",
  ExternalAPI: "http://www.geoplugin.net/json.gp?ip=2409:4072:e8b:f84d:993d:dd8:3d90:5f5d",
  url: '',
  IMAGEURL: 'https://swap-rent-product-api-h8zru.ondigitalocean.app/product/image/'
};
